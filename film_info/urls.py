from django.conf.urls import url
from film_info import views

urlpatterns = [
    url(r'^$', views.home, name='home_page'),
]

