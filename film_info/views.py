from django.shortcuts import render_to_response
import urllib.request as urllib, json, sys, requests
from .functions import form_url_for_query

#--------------------------------------------------------------------------------
# This is the (only) view in this application, this view both collects data from
# both the iPlayer and TMDB

def home(request):
# Collecting / parsing data from the BBC json site about current films.
  url_BBC = \
          "http://www.bbc.co.uk/tv/programmes/formats/films/player/episodes.json"
  response_BBC = urllib.urlopen(url_BBC)
  data_BBC = json.loads(response_BBC.readall().decode('utf-8'))
# Count is used for while loop instead of a for loop because the count is also
# used in referencing the arrays.
  count = len(data_BBC["episodes"]) - 1
# Data is stored in the details dict for each film then aggregated into the
# movies list.
  movies = []
  details = {}
  while (count > -1):
#   Simmilar to the above data is collected from TMDB url - the url is formatted
#   in the functions.py file.
    url_MovieDB = form_url_for_query(data_BBC, count)
    response_MovieDB = urllib.urlopen(url_MovieDB)
    data_MovieDB = json.loads(response_MovieDB.readall().decode('utf-8'))
#   if loop is used to determine if any results are found. If no results are
#   found from TMDB, then data from the iplayer is collected and displayed
#   without the rating data
    if data_MovieDB['results']:
      details = {
        'title': data_MovieDB['results'][0]['title'],
        'votes': data_MovieDB['results'][0]['vote_average'],
        'overview': data_MovieDB['results'][0]['overview'],
        'poster_url': "https://image.tmdb.org/t/p/original" + \
                                       data_MovieDB['results'][0]['poster_path'],
        'release_date': data_MovieDB['results'][0]['release_date'],
        'backdrop': data_MovieDB['results'][0]['backdrop_path'],
        'iplayer_link': "http://www.bbc.co.uk/iplayer/episode/" + \
                                 data_BBC['episodes'][count]['programme']['pid'],
        'availability': data_BBC['episodes'][count]['programme']\
                                                              ['available_until']
      }
    else:
      details = {
        'title': data_BBC["episodes"][count]['programme']['title'],
        'overview': data_BBC["episodes"][count]['programme']['short_synopsis'],
        'poster_url': "http://ichef.bbci.co.uk/images/ic/1200x675/" + \
                      data_BBC["episodes"][count]['programme']['image']['pid'] + \
                                                                          ".jpg",
        'iplayer_link': "http://www.bbc.co.uk/iplayer/episode/" + \
                                 data_BBC['episodes'][count]['programme']['pid'],
        'availability': data_BBC['episodes'][count]['programme']['media']\
                                                                     ['expires'],
      }
#   This is a workaround the iPlayer data not containing any value if
#   availability is 1 year or more into the future. If no data is returned
#   the below statement will fail and the entry for 'availability' will be hard
#   coded.
    try:
      details['availability'] = \
                             data_BBC['episodes'][count]['programme']['media']\
                                                   ['expires'].split( 'T', 1 )[0]
    except:
      details['availability'] = "1 year +"
    movies.append(details)
    count = count - 1
# The above movies list (full of details dictionaries) is passed to the 
# index.html page
  return render_to_response('film_info/index.html', {'movies': movies})

