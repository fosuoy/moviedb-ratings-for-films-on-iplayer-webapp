from iplayer.settings import MOVIEDB_API_KEY
#--------------------------------------------------------------------------------
# This function was moved out of the file views.py for cleanliness. It takes
# the output of the JSON from the BBC site and processes it for the name of the
# film, then forming the URL for the TMDB query.
def form_url_for_query(data_BBC, count):
    base_url_MovieDB = "https://api.themoviedb.org/3/search/movie?api_key="
    name_readable = data_BBC["episodes"][count]["programme"]["title"]
    name = name_readable.replace(" ", "+")
    query = "&query=%s" % name
    url_MovieDB = "%s%s%s" % ( base_url_MovieDB, MOVIEDB_API_KEY, query )
    return url_MovieDB
